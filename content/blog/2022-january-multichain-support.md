+++
date = "2022-02-01"
draft = false
title = "Hub20 - Release 0.4.0"
slug = "hub20-040"
image = false
comments = false	# set false to hide Disqus
share = true	# set false to hide share buttons
menu= "posts"		# set "main" to add this content to the main menu
author = "Raphael Lullis"
summary = """

Are we multichain yet? Yes, we are!

New year and a new release for Hub20 with something big: support for
any EVM-compatible blockchain means a lot more options for accepting
payments and makes it easier to board on the myriad of Ethereum's
scaling projects. Polygon? xDai? Optimistic Roll-ups? All of them?
"""
+++


Hey everyone! One of the new year's resolutions was to use this blog
to make more frequent updates about new releases and to show all the
new features that we are getting on Hub20. The big highlight of this
release is definitely the expansion to support more blockchains beyond
Ethereum, but there is a lot more interesting things to cover, so
let's get to it.

## A Wider Web3

Hub20 started focused exclusively on Ethereum for its base layer and
integrated [Raiden](https://raiden.network) for its off-chain scaling
capabilities. And while we still personally believe in the overall
goals from both projects - i.e, Ethereum being the network with viable
decentralization and affordable nodes, Raiden being the network to
make fast and cheap payments feasible - the undeniable reality is that
Ethereum popularity has priced out anyone that wants to use it as a
platform to accept payments. Even with [Raiden's latest
release](https://medium.com/raiden-network/coruscant-and-krittika-the-new-raiden-releases-on-mainnet-7c68d1e6177b),
the setup and onboarding costs are still too high to be attractive for
larger audiences. It makes very little sense to try to accept payments
with crypto when it costs tens of dollars to make a transfer, and It's
going to be hard to convince anyone to join a payment network that promises
near-zero fees when you'd be spending hundreds of dollars worth of ETH
when opening up a payment channel.

Some of our users asked about the possibility of making a version of
Hub20 that could focus on [Binance Smart
Chain](https://www.binance.org/en/smartChain). Given that BSC uses the
same [RPC API](https://eth.wiki/json-rpc/api) as Ethereum, and that
all our communication with Ethereum blockchain happens through this
API, we quickly realized that not only we could support BSC, but we
could easily add support for any blockchain that adopts the protocol
and that works with ethereum's web3 client library. A
not-so-complicated refactoring later and we can now proudly say that
you can run a hub with any combination of chains and networks at the
same time. No matter if your potential customers are on
Binance/[Polygon](https://polygon.technology)/[xDAI](https://xdaichain.com),
you can accept payments from all of them.

The coolest thing about this, though, is how this feature lets us
support other scaling solutions for Ethereum! It's been long
established that Ethereum is going to rely on a [roll-up
centric](https://ethereum-magicians.org/t/a-rollup-centric-ethereum-roadmap/4698)
strategy to scale, and Optimistic Roll-ups such as
[Optimism](https://www.optimism.io/) and
[Arbitrum](https://arbitrum.io) are already web3-compatible. All that
a hub operator will have to do now is configure the correct entry in
the admin panel, and in seconds they will be able to send and receive
token transfers there.


### More than one blockchain? How about more than one Raiden node?

This expansion into multiple chains is also going to be beneficial for
those that want to use Raiden. The Raiden team is also working to
allow the client to connect directly to Arbitrum, so as soon as that
is possible, the costs to onboard and create channels are going to
drop significantly and hub users will be able to make transfers at
almost zero cost.

Having Raiden deployed on multiple chains and roll-ups bring the
possibility of using it as a cross-rollup and even as a cross-chain
bridge. It's not difficult to imagine a future where someone wants to
move tokens between, e.g Binance and Arbitrum, and having Raiden as
the bridge would allow for atomic token swaps.

As part of this scenario, we also made it possible for hub operators
to connect to more than one single raiden node at the same time. We
hope to make it even easier for operators to launch new and manage new
Raiden nodes without having to setup beforehand.

## Token List support

Just like with the case of multi-blockchains, this release also brings
lots of improvements on the support for any token that is compliant to
the ERC20 interface. The previous version of Hub20 worked on the
assumption that only the hub operator could define what tokens could
be tracked. This new release makes things a lot more flexible:

Hub Operators can load information about any set of token that is
published web URL that follows the [token
list](https://tokenlists.org/) schema. Want to load ~1000 tokens that
are tracked by [MyCrypto
Wallet](https://tokenlists.org/token-list?url=https://uniswap.mycryptoapi.com/),
or just want to get details about the [tokens supported by
Optimism](https://tokenlists.org/token-list?url=https://static.optimism.io/optimism.tokenlist.json)?
Easy! Operators can add tokens individually as well. The API now can accept
token entries, and it will verify if the address is compliant with
the ERC20 interface.

Users can now also manage their own token lists. This not only gives
them freedom to create their own portfolio, but it also gives the
chance to make use-specific token lists. If you are a merchant that
wants to accept payments only with stabletokens but also wants to
track tokens related to DeFI projects, you can easily manage these
separately.


## General improvements

The REST API now provides a lot more information about the token and
the current status of the blockchain. Whenever you are using the
[frontend](https://app.hub20.io) to withdraw funds from the hub, you
will get the estimated transfer costs.

The API also now has a lot more powerful filters. You can filter the
lists by chain. You can find only stable currencies that are pegged to
specific fiat currencies. You can even find all the tokens that are
wrapper of a native crypto (W-ETH, WBTC, renBTC, etc...).

The frontend has seen a lot of improvements in terms of general
responsiveness and some small UX-details ([identicon
blockies](https://en.wikipedia.org/wiki/Identicon) for your profile!)

Hub Operators will be happy to know that the process that listen to
blockchain events was vastly simplified and that the number of queries
has been greatly reduced. The previous implementation was causing a
huge number of RPC requests when lots of tokens were involved, but the
current implementation removed this bottleneck and the hub is making
one single request per new block.


## What is coming next

The next release will be focused in the [checkout
 widget](https://gitlab.com/mushroomlabs/hub20/checkout20) and on the
 management of Raiden channels. We also need to play some catch-up
 with the [documentation site](https://docs.hub20.io) to make it
 easier for newcomers to get start with their hubs.

You can always follow what's on our
 roadmap by checking our [Project Management
 page](https://tree.taiga.io/project/lullis-mushroomlabshub20/).

 ---

That is it for this month. We hope you get to take a look at Hub20 and
get as excited about the new features as we are. Come say hi at our
[Matrix room](https://matrix.to/#/#hub20-support:communick.com) and
please let us know what you think!
